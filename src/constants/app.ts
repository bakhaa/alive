export const SIDE_BAR_WIDTH = 260;
export const CHAT_WIDTH = 340;

export const COLORS = {
  white: '#fff',
  red: '#f43',
  yellow: '#F5F800',
  blue: '#28ADCA',
  silver: '#F4F6FC',
  green: '#29B81E',
  brigBlue: '#8BF7FD',
  orange: '#E6AA02',
  brightGreen: '#9FF78B',
  purple: '#9147ff',
  gold: '#e4ae39',
};
