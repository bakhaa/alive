import clsx from 'clsx';
import { useTranslation } from 'react-i18next';
import { useHistory, useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Drawer,
  List,
  Divider,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@material-ui/core';

import AccountBoxIcon from '@material-ui/icons/AccountBox';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';

import { SIDE_BAR_WIDTH } from '../../constants/app';

type Props = {
  isSideBarOpen: boolean;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {
      width: SIDE_BAR_WIDTH,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      width: SIDE_BAR_WIDTH,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      // justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    logo: {
      fontSize: 13,
      fontWeight: 'bold',
    },
    logoLight: {
      color: theme.palette.type === 'dark' ? '#fff' : '#54547f',
    },
  })
);

export default function SideBar({ isSideBarOpen }: Props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const history = useHistory();
  const location = useLocation();

  const pages = [
    {
      title: 'nav.wallet',
      href: '/',
      exact: true,
      IconComponent: (props: any) => <CardGiftcardIcon {...props} />,
      requireAuth: true,
    },
    {
      title: t('nav.finance'),
      href: '/finance',
      exact: false,
      IconComponent: (props: any) => <AccountBalanceWalletIcon {...props} />,
      requireAuth: true,
    },
    {
      title: 'nav.account',
      href: '/users',
      exact: false,
      IconComponent: (props: any) => <AccountBoxIcon {...props} />,
      requireAuth: true,
    },
  ];

  const NavLink = ({ IconComponent, title, href, exact }: any) => {
    let isActivePage = href === location.pathname;

    if (!exact) {
      const _href = href.split('/')[1];

      const splitedRouter = location.pathname?.split('/');
      const routes = splitedRouter.splice(1, splitedRouter.length - 1);

      if (routes.some((value) => value === _href)) isActivePage = true;
    }

    return (
      <ListItem selected={isActivePage} button onClick={() => history.push(href)}>
        <ListItemIcon>
          <IconComponent color={isActivePage ? 'primary' : 'inherit'} />
        </ListItemIcon>
        <ListItemText primary={title} />
      </ListItem>
    );
  };

  return (
    <Drawer
      variant="permanent"
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: isSideBarOpen,
        [classes.drawerClose]: !isSideBarOpen,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: isSideBarOpen,
          [classes.drawerClose]: !isSideBarOpen,
        }),
      }}
    >
      <div className={classes.toolbar}>
        <Link to="/">
          <Typography className={classes.logo} noWrap color="primary">
            Logo.<span className={classes.logoLight}>com</span>
          </Typography>
        </Link>
      </div>
      <Divider />

      <List>
        {pages.map((page, index) => (
          <NavLink key={index} {...page} />
        ))}
      </List>
    </Drawer>
  );
}
