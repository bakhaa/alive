import { useContext, useState } from 'react';
import clsx from 'clsx';
import { useTranslation } from 'react-i18next';
import { NavLink, useLocation } from 'react-router-dom';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  AppBar,
  Toolbar,
  IconButton,
  Menu,
  MenuItem,
  Button,
  Tooltip,
  Typography,
} from '@material-ui/core';

import TranslateIcon from '@material-ui/icons/Translate';
import MenuIcon from '@material-ui/icons/Menu';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import BrightnessHighIcon from '@material-ui/icons/BrightnessHigh';
import Brightness4Icon from '@material-ui/icons/Brightness4';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import { ThemeContext } from '../../context/theme';
import { COLORS, SIDE_BAR_WIDTH } from '../../constants/app';

type Props = {
  handleSideBarOpen: () => any;
  handleSideBarClose: () => any;
  isSideBarOpen: boolean;
};

type AvalibleLanguages = 'ru' | 'en';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      marginLeft: 73,
      width: `calc(100% - ${73}px)`,
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      borderBottom:
        theme.palette.type === 'dark'
          ? '1px solid rgba(255, 255, 255, 0.12)'
          : '1px solid rgba(0, 0, 0, 0.12)',
    },
    appBarShift: {
      marginLeft: SIDE_BAR_WIDTH,
      width: `calc(100% - ${SIDE_BAR_WIDTH}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    actions: {},
    grow: {
      flexGrow: 1,
    },
    languageTitle: {
      margin: '0px 4px 0px 8px',
    },
    nav: {
      display: 'flex',
      '& > *': {
        marginRight: theme.spacing(2),
      },
    },
    navLink: {
      color: theme.palette.type === 'dark' ? '#fff' : theme.palette.text.primary,
      '&.active': {
        color: COLORS.purple,
      },
      '& > p': {
        fontWeight: 'bold',
      },
    },
  })
);

const financeNav = [
  { link: '/finance', title: 'Аналитика' },
  { link: '/finance/transactions', title: 'Транзакции' },
  { link: '/finance/accounts', title: 'Счета' },
  { link: '/finance/categories', title: 'Категории' },
];

const getFullTextLanguage = (lng: AvalibleLanguages) => {
  switch (lng) {
    case 'ru':
      return 'Русский';
    default:
      return 'English';
  }
};

const languages: AvalibleLanguages[] = ['en', 'ru'];

export default function TopBar({ handleSideBarOpen, handleSideBarClose, isSideBarOpen }: Props) {
  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const { pathname } = useLocation();

  const { isDarkTheme, toggleTheme } = useContext(ThemeContext);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const handleLanguageMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleLanguageMenuClose = () => {
    setAnchorEl(null);
  };

  const switchLanguage = (lng: AvalibleLanguages) => {
    i18n.changeLanguage(lng);
    handleLanguageMenuClose();
  };

  const changeTheme = () => toggleTheme();

  const getNavigation = () => {
    const section = pathname.split('/')[1];

    if (section === 'finance') {
      return financeNav.map((item, id) => (
        <NavLink className={classes.navLink} activeClassName="active" exact key={id} to={item.link}>
          <Typography>{item.title}</Typography>
        </NavLink>
      ));
    }

    return null;
  };

  const isMenuOpen = Boolean(anchorEl);
  const selectedLanguage = i18n.language as AvalibleLanguages;

  const renderLanguageMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleLanguageMenuClose}
    >
      {languages.map((item) => (
        <MenuItem
          key={item}
          selected={selectedLanguage === item}
          onClick={() => switchLanguage(item)}
        >
          {getFullTextLanguage(item)}
        </MenuItem>
      ))}
    </Menu>
  );

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: isSideBarOpen,
      })}
    >
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={!isSideBarOpen ? handleSideBarOpen : handleSideBarClose}
          edge="start"
          className={classes.menuButton}
        >
          {!isSideBarOpen ? <MenuIcon /> : <ChevronLeftIcon />}
        </IconButton>
        <div className={classes.nav}>{getNavigation()}</div>

        <div className={classes.grow} />
        <div className={classes.actions}>
          <Tooltip title={t('app.changeLanguage') as string}>
            <Button onClick={handleLanguageMenuOpen}>
              <TranslateIcon />
              <div className={classes.languageTitle}>{getFullTextLanguage(selectedLanguage)}</div>
              <ExpandMoreIcon />
            </Button>
          </Tooltip>

          <Tooltip title={t('app.changeTheme') as string}>
            <IconButton onClick={changeTheme}>
              {isDarkTheme ? <BrightnessHighIcon /> : <Brightness4Icon />}
            </IconButton>
          </Tooltip>
        </div>
        {renderLanguageMenu}
      </Toolbar>
    </AppBar>
  );
}
