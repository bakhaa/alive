import React, { ReactNode } from 'react';

type Props = {
  children?: ReactNode;
  title?: string;
  description?: string;
};

const PageLayout = ({ children, title = 'Default title', description = '' }: Props) => (
  <>
    {/* <Head>
      <title>{title} | AppTitle</title>
      <meta name="description" content={description} />
    </Head> */}

    {children}
  </>
);

export default PageLayout;
