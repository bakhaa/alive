import { ReactNode, useState } from 'react';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';

import TopBar from '../TopBar';
import SideBar from '../SideBar';

type Props = {
  children: ReactNode;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: 'none',
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  })
);

export default function MainLayout({ children }: Props) {
  const classes = useStyles();

  const [isSideBarOpen, setSideBarOpen] = useState(false);

  const handleSideBarOpen = () => setSideBarOpen(true);
  const handleSideBarClose = () => setSideBarOpen(false);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <TopBar
        isSideBarOpen={isSideBarOpen}
        handleSideBarOpen={handleSideBarOpen}
        handleSideBarClose={handleSideBarClose}
      />
      <SideBar isSideBarOpen={isSideBarOpen} />
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}
      </main>
    </div>
  );
}
