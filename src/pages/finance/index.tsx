import React from 'react';
import { Switch, Route } from 'react-router-dom';

import CategoriesPage from './categories';
import AnaliticsPage from './analitics';
import TransactionsPage from './transactions';
import AccountsPage from './accounts';

export default function GmailTreeView() {
  return (
    <Switch>
      <Route path="/finance" exact>
        <AnaliticsPage />
      </Route>
      <Route path="/finance/transactions">
        <TransactionsPage />
      </Route>
      <Route path="/finance/accounts">
        <AccountsPage />
      </Route>
      <Route path="/finance/categories">
        <CategoriesPage />
      </Route>
    </Switch>
  );
}
