import React from 'react';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { ButtonGroup, Button } from '@material-ui/core';

import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    actions: {
      maxWidth: 700,
      '& > *': {
        marginRight: theme.spacing(1),
      },
    },
  })
);

export default function CategoryActions() {
  const classes = useStyles();

  return (
    <div className={classes.actions}>
      <ButtonGroup disableRipple color="primary" size="small">
        <Button disabled>
          <EditIcon fontSize="small" />
        </Button>
        <Button disabled>
          <DeleteIcon fontSize="small" />
        </Button>
        <Button>
          <PlaylistAddIcon fontSize="small" />
        </Button>
        <Button>
          <VisibilityIcon fontSize="small" />
        </Button>
        <Button>
          <VisibilityOffIcon fontSize="small" />
        </Button>
      </ButtonGroup>
    </div>
  );
}
