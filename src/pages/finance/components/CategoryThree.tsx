import React from 'react';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import TreeItem, { TreeItemProps } from '@material-ui/lab/TreeItem';
import { Typography, SvgIconProps, Checkbox, FormControlLabel } from '@material-ui/core';

import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import { ICategory } from '../../../interfaces/finance/category';
import { categories } from '../data';
import { getCategoryIcon } from '../../../utils/finance/category';

declare module 'csstype' {
  interface Properties {
    '--tree-view-color'?: string;
    '--tree-view-bg-color'?: string;
  }
}

type StyledTreeItemProps = TreeItemProps & {
  color?: string;
  labelIcon: React.ElementType<SvgIconProps>;
  labelText: string;
  income?: boolean;
  outcome?: boolean;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      color: theme.palette.text.secondary,
      '& $content': {
        color: 'var(--tree-view-color)',
      },
      '&:hover > $content': {
        backgroundColor: theme.palette.action.hover,
      },
      '&:focus > $content, &$selected > $content': {
        backgroundColor: theme.palette.action.hover,
        outline: '1px dashed rgba(112, 112, 112, 0.3)',
      },
      '&:focus > $content $label, &:hover > $content $label, &$selected > $content $label': {
        backgroundColor: 'transparent!important',
      },
    },
    content: {
      color: theme.palette.text.secondary,
      paddingRight: theme.spacing(1),
      fontWeight: theme.typography.fontWeightMedium,
      '$expanded > &': {
        fontWeight: theme.typography.fontWeightRegular,
      },
    },
    group: {
      marginLeft: 0,
      '& $content': {
        paddingLeft: theme.spacing(2),
      },
    },
    expanded: {},
    selected: {},
    label: {
      fontWeight: 'inherit',
      color: 'inherit',
    },
    labelRoot: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0.5, 0.5),
    },
    labelIcon: {
      marginRight: theme.spacing(1),
    },
    labelText: {
      fontWeight: 'inherit',
      flexGrow: 1,
    },
    labelCheckbox: {
      color: theme.palette.text.secondary,
    },
    labelCheckboxText: {
      fontSize: '0.875rem',
    },
    treeView: {
      flexGrow: 1,
      maxWidth: 700,
      margin: theme.spacing(2, 0),
    },
  })
);

function StyledTreeItem({
  labelText,
  labelIcon: LabelIcon,
  color,
  income = false,
  outcome = false,
  ...other
}: StyledTreeItemProps) {
  const classes = useStyles();

  return (
    <TreeItem
      label={
        <div className={classes.labelRoot}>
          {/* <DragIndicatorIcon /> */}
          <LabelIcon color="inherit" className={classes.labelIcon} />
          <Typography variant="body2" className={classes.labelText}>
            {labelText}
          </Typography>

          <Typography className={classes.labelCheckbox} variant="caption" color="inherit">
            <FormControlLabel
              control={<Checkbox defaultChecked={outcome} color="secondary" />}
              label="В расходе"
              classes={{ label: classes.labelCheckboxText }}
            />

            <FormControlLabel
              control={<Checkbox defaultChecked={income} color="primary" />}
              label="В доходе"
              classes={{ label: classes.labelCheckboxText }}
            />
          </Typography>
        </div>
      }
      style={{
        '--tree-view-color': color,
      }}
      classes={{
        root: classes.root,
        content: classes.content,
        expanded: classes.expanded,
        selected: classes.selected,
        group: classes.group,
        label: classes.label,
      }}
      {...other}
    />
  );
}

export default function CategoryThree() {
  const classes = useStyles();

  const renderCategories = (categories: ICategory[]) =>
    categories.map((category) => (
      <StyledTreeItem
        key={category.id}
        nodeId={`${category.id}`}
        labelText={category.title}
        income={category.income}
        outcome={category.outcome}
        labelIcon={getCategoryIcon(category.icon)}
        color={category.color || ''}
      >
        {category.childs ? renderCategories(category.childs) : null}
      </StyledTreeItem>
    ));

  return (
    <TreeView
      className={classes.treeView}
      defaultExpanded={['3', '5']}
      defaultCollapseIcon={<ArrowDropDownIcon />}
      defaultExpandIcon={<ArrowRightIcon />}
      defaultEndIcon={<div style={{ width: 24 }} />}
    >
      {renderCategories(categories)}
    </TreeView>
  );
}
