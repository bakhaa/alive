import React from 'react';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(2, 2),
    },
  })
);

export default function AnaliticsPage() {
  const classes = useStyles();

  return <Paper className={classes.root}>Home</Paper>;
}
