import React from 'react';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Button, Paper } from '@material-ui/core';

import CategoryActions from './components/CategoryActions';
import CategoryThree from './components/CategoryThree';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(2, 2),
    },
    actions: {
      maxWidth: 700,
      '& > *': {
        marginRight: theme.spacing(1),
      },
    },
  })
);

export default function CategoryPage() {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <CategoryActions />

      <CategoryThree />

      <div className={classes.actions}>
        <Button variant="outlined">Сохранить</Button>
        <Button variant="outlined" color="secondary">
          Отмена
        </Button>
      </div>
    </Paper>
  );
}
