import React, { ReactNode, useEffect, useState } from 'react';

export const ThemeContext = React.createContext({
  isDarkTheme: false,
  toggleTheme: () => {},
});

export function ThemeProvider({ children }: { children: ReactNode }) {
  const [isDarkTheme, setDark] = useState(false);

  useEffect(() => {
    const appSetting = window.localStorage.getItem('app');
    if (appSetting) {
      const parsedAppSettings = JSON.parse(appSetting);
      setDark(parsedAppSettings.isDarkTheme);
    }
  }, []);

  const toggleTheme = () => {
    setDark(!isDarkTheme);
    window.localStorage.setItem('app', JSON.stringify({ isDarkTheme: !isDarkTheme }));
  };

  return (
    <ThemeContext.Provider value={{ isDarkTheme, toggleTheme }}>{children}</ThemeContext.Provider>
  );
}

export default ThemeProvider;
