import i18n from "i18next";

import { initReactI18next } from "react-i18next";
import locale from "./locale.json";

i18n.use(initReactI18next).init({
  fallbackLng: "ru",
  debug: false,
  resources: locale,
  lng: "ru",
  interpolation: {
    escapeValue: false // not needed for react as it escapes by default
  }
});

export default i18n;
