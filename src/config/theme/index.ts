import { createMuiTheme, useTheme } from '@material-ui/core/styles';
import { PaletteOptions } from '@material-ui/core/styles/createPalette';
import { Shadows } from '@material-ui/core/styles/shadows';
import { lighten } from '@material-ui/core/styles';

import { COLORS } from '../../constants/app';

// Create a theme instance.
const theme = (paletteType: PaletteOptions['type']) => {
  const isDark = paletteType === 'dark';

  const defaultBackground = isDark ? '#18191a' : '#f3f3f3';

  return createMuiTheme({
    palette: {
      type: paletteType,
      primary: {
        main: COLORS.purple,
      },
      secondary: {
        main: COLORS.red,
      },
      background: {
        default: defaultBackground,
      },
    },
    overrides: {
      MuiButton: {
        root: {
          textTransform: 'none',
        },
      },
      MuiPaper: {
        root: {
          backgroundColor: isDark ? '#242526' : '#fff',
        },
      },
      MuiAppBar: {
        colorPrimary: {
          color: !isDark ? '#000' : '#fff',
          backgroundColor: isDark ? '#131415' : '#ececec',
        },
      },
      MuiListItem: {
        root: {
          '&$selected': {
            backgroundColor: isDark ? '#2f2f2f' : '#e4e4e4',
          },
        },
      },
      MuiCssBaseline: {
        '@global': {
          a: {
            textDecoration: 'none',
            color: COLORS.purple,
          },
        },
      },
    },
    shadows: Array(25).fill('none') as Shadows,
    props: {
      MuiButtonBase: {
        disableRipple: true,
      },
    },
  });
};

export default theme;
