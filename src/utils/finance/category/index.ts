import InfoIcon from '@material-ui/icons/Info';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import Label from '@material-ui/icons/Label';
import DriveEtaIcon from '@material-ui/icons/DriveEta';
import LocalGasStationIcon from '@material-ui/icons/LocalGasStation';
import LocalTaxiIcon from '@material-ui/icons/LocalTaxi';
import LanguageIcon from '@material-ui/icons/Language';
import ReceiptIcon from '@material-ui/icons/Receipt';
import FastfoodIcon from '@material-ui/icons/Fastfood';

import { CategoryIcons } from '../../../interfaces/finance/category';

export const getCategoryIcon = (icon: CategoryIcons) => {
  switch (icon) {
    case 'AccountBalanceWalletIcon':
      return AccountBalanceWalletIcon;
    case 'FastfoodIcon':
      return FastfoodIcon;
    case 'ReceiptIcon':
      return ReceiptIcon;
    case 'LanguageIcon':
      return LanguageIcon;
    case 'DriveEtaIcon':
      return DriveEtaIcon;
    case 'LocalGasStationIcon':
      return LocalGasStationIcon;
    case 'LocalTaxiIcon':
      return LocalTaxiIcon;
    case 'Label':
      return Label;

    default:
      return InfoIcon;
  }
};
