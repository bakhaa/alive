import React, { useContext } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';

import { Finance, Home, Users } from './pages';
import MainLayout from './components/Layout/Main';
import createTheme from './config/theme';
import { ThemeContext } from './context/theme';
import './config/i18n';

export default function App() {
  const { isDarkTheme } = useContext(ThemeContext);
  const theme = createTheme(isDarkTheme ? 'dark' : 'light');

  return (
    <MuiThemeProvider theme={theme}>
      <Router>
        <MainLayout>
          <Switch>
            <Route path="/finance">
              <Finance />
            </Route>
            <Route path="/users">
              <Users />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </MainLayout>
      </Router>
    </MuiThemeProvider>
  );
}
