export type CategoryIcons =
  | 'AccountBalanceWalletIcon'
  | 'FastfoodIcon'
  | 'InfoIcon'
  | 'ReceiptIcon'
  | 'LanguageIcon'
  | 'DriveEtaIcon'
  | 'LocalGasStationIcon'
  | 'LocalTaxiIcon'
  | 'Label';

export interface ICategory {
  id: number;
  income: boolean;
  outcome: boolean;
  parentId: number | null;
  isParent: boolean;
  title: string;
  icon: CategoryIcons;
  color: string | null;
  childs?: ICategory[];
  // createdAt: string;
}
